# Trinkets 📖 tech blog

[![Project name][project-img]][project-url]

Welcome to the Shiny Trinkets blog!

All the posts are available in the [posts/](posts/) folder.

They are also published on https://trinkets.tech/ 🌐

[project-img]: https://badgen.net/badge/%E2%AD%90/Trinkets/4B0082
[project-url]: https://github.com/ShinyTrinkets
